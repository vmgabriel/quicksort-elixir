# Implementacion algoritmo quickSort en Elixir

[![Built with Spacemacs](https://cdn.rawgit.com/syl20bnr/spacemacs/442d025779da2f62fc86c2082703697714db6514/assets/spacemacs-badge.svg)](http://spacemacs.org)

- Ordena de Mayor a Menor
- Uso de recursividad
- Cambios minimos para que se ordene de Menor a Mayor

## Ejecucion

1. Se Carga iex
2. Se ejecuta
```elixir
Ejecucion.main()
```
3. Se obtienen resultados similares a:

![Ejecucion Algoritmo Ordenado](img/algoritmo_quick.png "Ejecucion Algoritmo Ordenado")
