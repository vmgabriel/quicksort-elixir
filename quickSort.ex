defmodule Quicksort do
  def sort([]), do: [] # Cuando este Vacio
  def sort([head | tail]) do
    # Particiono cabeza y resto del arreglo
    { before, aft } = Enum.partition(tail, &(&1 < head))
    # Tomo cada uno de los elementos, los comparo y lo pongo en la mitad del
    # antes y el despues, el antes puede ser vacio, lo mismo el despues
    sort(before) ++ [head] ++ sort(aft)
  end
end

defmodule Ejecucion do
  def main do
    IEx.configure(inspect: [charlists: :as_lists])
    lista_elementos = [84, 32, 43, 12, 4, 49, 34, 5, 53, 4, 43, 100, 21, 2]

    IO.puts("---- Elementos No Ordenados ----")
    IO.inspect(lista_elementos, charlists: :as_lists)
    lista_elementos = Quicksort.sort(lista_elementos)
    IO.puts("\n---- Elementos Debidamente Organizados ----")
    IO.inspect(lista_elementos, charlists: :as_lists)
    :ok
  end
end
